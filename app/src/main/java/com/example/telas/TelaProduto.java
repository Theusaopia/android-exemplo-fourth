package com.example.telas;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class TelaProduto extends AppCompatActivity {

    EditText edCodigo, edDescricao, edValor, edEstoque, edFornecedor, edTelefoneFornecedor;
    private static final int REQUEST_CALL = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_produto);

        edCodigo = (EditText) findViewById(R.id.edCodigo);
        edDescricao = (EditText) findViewById(R.id.edDescricao);
        edValor = (EditText) findViewById(R.id.edPreco);
        edEstoque = (EditText) findViewById(R.id.edEstoque);
        edFornecedor = (EditText) findViewById(R.id.edFornecedor);
        edTelefoneFornecedor = (EditText) findViewById(R.id.edTelefoneFornecedor);

        Intent it = getIntent();
        Produto p = (Produto) it.getSerializableExtra("prodEdicao");

        if(p != null) {
            resetEditText(p);
        }
    }

    public void resetEditText(Produto p) {
        edCodigo.setText(""+p.getCodigo());
        edCodigo.setEnabled(false);
        edDescricao.setText(p.getDescricao());
        edValor.setText(String.valueOf(p.getValor()));
        edEstoque.setText(String.valueOf(p.getEstoque()));
        edFornecedor.setText(p.getNomeFornecedor());
        edTelefoneFornecedor.setText(p.getTelefoneFornecedor());
    }

    public void confirmar(View view) {
        Produto p = new Produto();

        p.setCodigo(Integer.parseInt(edCodigo.getText().toString()));
        p.setDescricao(edDescricao.getText().toString());
        p.setValor(Double.parseDouble(edValor.getText().toString()));
        p.setEstoque(Double.parseDouble(edEstoque.getText().toString()));
        p.setNomeFornecedor(edFornecedor.getText().toString());
        p.setTelefoneFornecedor(edTelefoneFornecedor.getText().toString());

        Intent resposta = new Intent();
        resposta.putExtra("produto", p);
        setResult(RESULT_OK, resposta);
        finish();
    }

    public void cancelar(View view) {
        setResult(RESULT_CANCELED);
        finish();
    }

    public void ligar(View view) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + edTelefoneFornecedor.getText().toString()));
        startActivity(intent);
    }

}