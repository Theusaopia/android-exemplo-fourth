package com.example.telas;

import java.io.Serializable;

public class Produto implements Serializable {
    private int codigo;
    private String descricao, nomeFornecedor, telefoneFornecedor;
    private double estoque, valor;

    public Produto(int codigo, String descricao, String nomeFornecedor, String telefoneFornecedor, double estoque, double valor) {
        this.codigo = codigo;
        this.descricao = descricao;
        this.nomeFornecedor = nomeFornecedor;
        this.telefoneFornecedor = telefoneFornecedor;
        this.estoque = estoque;
        this.valor = valor;
    }

    public Produto() {

    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getNomeFornecedor() {
        return nomeFornecedor;
    }

    public void setNomeFornecedor(String nomeFornecedor) {
        this.nomeFornecedor = nomeFornecedor;
    }

    public String getTelefoneFornecedor() {
        return telefoneFornecedor;
    }

    public void setTelefoneFornecedor(String telefoneFornecedor) {
        this.telefoneFornecedor = telefoneFornecedor;
    }

    public double getEstoque() {
        return estoque;
    }

    public void setEstoque(double estoque) {
        this.estoque = estoque;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public void atualizarProduto(String descricao, String nomeFornecedor, String telefoneFornecedor, double estoque, double valor) {
        this.descricao = descricao;
        this.nomeFornecedor = nomeFornecedor;
        this.telefoneFornecedor = telefoneFornecedor;
        this.estoque = estoque;
        this.valor = valor;
    }
}
