package com.example.telas;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {
    final int TELA_PRODUTO = 10;
    final int EDITAR_PRODUTO = 11;

    ListView produtos;
    ArrayAdapter adapter;

    List<Produto> produtosList;
    List<Integer> selecionados = new LinkedList<>();

    int pos;

    class ProdutoAdapter extends ArrayAdapter<Produto> {
        List<Produto> listaProdutos;

        public ProdutoAdapter(List<Produto> produtos) {
            super(MainActivity.this, R.layout.activity_item_lista_produtos, produtos);
            listaProdutos = produtos;
        }

        @RequiresApi(api = Build.VERSION_CODES.M)
        @Override
        public View getView(int pos, View recicled, ViewGroup parent) {
            if(recicled == null) {
                recicled = getLayoutInflater().inflate(R.layout.activity_item_lista_produtos, null);
            }

            Produto p = listaProdutos.get(pos);

            int rot = getWindowManager().getDefaultDisplay().getRotation();

            if(rot == Surface.ROTATION_180 || rot == Surface.ROTATION_270) {
                recicled = getLayoutInflater().inflate(R.layout.activity_item_lista_produtos_landscape, null);

                ((TextView) recicled.findViewById(R.id.ilCodigo)).setText(String.valueOf(p.getCodigo()));
                ((TextView) recicled.findViewById(R.id.ilNomeProduto)).setText(p.getDescricao());
                ((TextView) recicled.findViewById(R.id.ilEstoque)).setText(String.valueOf(p.getEstoque()));
                ((TextView) recicled.findViewById(R.id.ilValor)).setText(String.valueOf(p.getValor()));
                ((TextView) recicled.findViewById(R.id.ilNomeFornecedor)).setText(p.getNomeFornecedor());
                ((TextView) recicled.findViewById(R.id.ilTelefoneFornecedor)).setText(p.getTelefoneFornecedor());
            }

            if(rot == Surface.ROTATION_0 || rot == Surface.ROTATION_90){
                ((TextView) recicled.findViewById(R.id.ilCodigo)).setText(String.valueOf(p.getCodigo()));
                ((TextView) recicled.findViewById(R.id.ilNomeProduto)).setText(p.getDescricao());
                ((TextView) recicled.findViewById(R.id.ilEstoque)).setText(String.valueOf(p.getEstoque()));
                ((TextView) recicled.findViewById(R.id.ilValor)).setText(String.valueOf(p.getValor()));
            }



            if(selecionados.contains(pos)) {
                recicled.setBackgroundResource(android.R.color.holo_red_light);
            }else {
                recicled.setBackgroundResource(android.R.color.white);
            }

            return recicled;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(savedInstanceState != null) {
            produtosList = (ArrayList<Produto>) savedInstanceState.getSerializable("produtos");
        }

        if(produtosList == null) {
            produtosList = new ArrayList<>();
            produtosList.add(new Produto(403, "Pistola", "Torcida organizada do Corinthians", "40028922", 5, 2.50));
            produtosList.add(new Produto(985, "Limão", "Jacaré", "8745874", 45, 0.74));

        }

        produtos = (ListView) findViewById(R.id.lista);

        adapter = new ProdutoAdapter(produtosList);

        produtos.setAdapter(adapter);
        produtos.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);

        produtos.setOnItemClickListener(this);
    }

    @Override
    public void onSaveInstanceState(Bundle state) {
        super.onSaveInstanceState(state);
        state.putSerializable("produtos", (Serializable) produtosList);
    }

    public void novo(View view) {
        Intent intencao = new Intent(this, TelaProduto.class);
        startActivityForResult(intencao, TELA_PRODUTO);
    }

    @Override
    public void onActivityResult(int codigo, int status, Intent dados) {
        super.onActivityResult(codigo, status, dados);

        if(codigo == TELA_PRODUTO){
            if(status == RESULT_OK) { //adicionar na lista e atualizar o adapter
                Produto p = (Produto) dados.getSerializableExtra("produto");
                produtosList.add(p);
            }
        }
        if(codigo == EDITAR_PRODUTO) {
            if(status == RESULT_OK) {
                Produto p = (Produto) dados.getSerializableExtra("produto");
                atualizarProduto(p);
            }
        }
    }

    public void editar(View v) {
        Intent editar = new Intent(this, TelaProduto.class);

        Produto of = produtosList.get(pos);

        editar.putExtra("prodEdicao", of);
        startActivityForResult(editar, EDITAR_PRODUTO);
    }

    public void remover(View view) {
        produtosList.remove(this.pos);
        adapter.notifyDataSetChanged();
        produtos.clearChoices();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
        selecionados.clear();
        selecionados.add(pos);
        this.pos = pos;

        adapter.notifyDataSetChanged();
    }

    public void atualizarProduto(Produto produto) {
        for(Produto p : produtosList) {
            if(p.getCodigo() == produto.getCodigo()) {
                p.atualizarProduto(produto.getDescricao(), produto.getNomeFornecedor(), produto.getTelefoneFornecedor(),
                        produto.getEstoque(), produto.getValor());
            }
        }
    }
}